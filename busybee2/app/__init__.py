#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# @Time    : 2020/12/10 19:02
# @Author  : Wang Feng
# @FileName: __init__.py.py
# @Software: PyCharm
from imp import reload

from flask import Flask

from app.settings import envs
from app.ext import init_ext, scheduler
from flask_cors import CORS


# 工厂模式
def create_app(env):
    """
    创建flask应用对象
    :param env: 配置模式的名字
    :return:
    """
    # 初始化flask应用对象
    app = Flask(__name__)

    # 初始化flask配置
    app.config.from_object(envs.get(env))

    # 支持跨域
    CORS(app, supports_credentials=True)

    # 初始化第三方应用
    init_ext(app)

    # 初始化路由
    from app.api import init_view
    init_view(app)

    scheduler.start()

    from app.schedule import tasks

    return app
