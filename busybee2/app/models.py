#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# @Time    : 2020/12/10 20:14
# @Author  : Wang Feng
# @FileName: models.py
# @Software: PyCharm
from dataclasses import dataclass
from datetime import datetime

from werkzeug.security import generate_password_hash

from app.ext import db


@dataclass
class BaseModel(db.Model):
    """模型基类，为每个模型补充创建时间与更新时间"""
    # id: int
    # create_time: datetime
    # update_time: datetime

    __abstract__ = True
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    create_time = db.Column(db.DateTime, default=datetime.now, doc='记录创建时间')  #
    update_time = db.Column(db.DateTime, default=datetime.now, onupdate=datetime.now, doc='记录的更新时间')

    def save(self):
        try:
            db.session.add(self)
            db.session.commit()
            return True
        except Exception as e:
            print(e)
            return False

    def delete(self):
        try:
            db.session.delete(self)
            db.session.commit()
            return True
        except Exception as e:
            print(e)
            return

    @classmethod
    def save_all(cls, lists):
        try:
            db.session.add_all(lists)
            db.session.commit()
            return True
        except Exception as e:
            print(e)
            return False


@dataclass
class TestCase(BaseModel):
    # epic: str
    # feature: str
    # story: str
    # title: str
    # page: str
    # marks: str
    # label: str
    # is_auto: str
    # has_test_script: str
    # steps: str
    # expects: str
    # description: str
    # prepare: str
    # severity: str
    # test_case_id: str
    # tester: str
    # test_case_full_name: str
    # test_case_pk: str

    """测试用例"""
    __tablename__ = "testcase"

    epic = db.Column(db.String(64), doc='项目')
    feature = db.Column(db.String(64), doc='模块')
    story = db.Column(db.String(64), doc='子模块')
    title = db.Column(db.String(64), doc='用例标题')
    page = db.Column(db.String(64), doc='页面')
    marks = db.Column(db.String(64), doc='标记')
    label = db.Column(db.String(64), doc='标签')
    is_auto = db.Column(db.Boolean(), default=True, doc='是否自动化')
    has_test_script = db.Column(db.Boolean(), default=False, doc='是否完成测试脚本编写')
    steps = db.Column(db.TEXT, doc='操作步骤')
    expects = db.Column(db.TEXT, doc='期望结果')
    description = db.Column(db.TEXT, doc='描述')
    prepare = db.Column(db.String(256), doc='前置条件')
    severity = db.Column(db.String(4), doc='优先级')
    test_case_id = db.Column(db.String(64), doc='测试编号')
    tester = db.Column(db.String(32), doc='测试人员')
    test_case_full_name = db.Column(db.String(512), doc='用例名称')
    test_case_pk = db.Column(db.String(128), doc='用例编码')

    # 用于序列化模型数据
    def to_dict(self):
        return {
            'id': self.id,
            'create_time': self.create_time,
            'update_time': self.update_time,
            'epic': self.epic,
            'feature': self.feature,
            'story': self.story,
            'title': self.title,
            'page': self.page,
            'marks': self.marks,
            'label': self.label,
            'is_auto': self.is_auto,
            'has_test_script': self.has_test_script,
            'steps': self.steps,
            'expects': self.expects,
            'description': self.description,
            'prepare': self.prepare,
            'severity': self.severity,
            'test_case_id': self.test_case_id,
            'tester': self.tester,
            'test_case_full_name': self.test_case_full_name,
            'test_case_pk': self.test_case_pk
        }


class TestScript(BaseModel):
    """测试脚本"""
    __tablename__ = 'testscript'

    author = db.Column(db.String(20), doc='作者')
    comments = db.Column(db.String(256), doc='注释')
    time = db.Column(db.DATETIME, doc='创建时间')
    epic = db.Column(db.String(64), doc='项目')
    feature = db.Column(db.String(64), doc='模块')
    story = db.Column(db.String(64), doc='子模块')
    title = db.Column(db.String(64), doc='用例标题')
    source_code = db.Column(db.TEXT, doc='用例脚本')
    test_case_doc = db.Column(db.TEXT, doc='')
    test_case_full_name = db.Column(db.String(512), doc='用例名称')
    test_case_id = db.Column(db.String(256), doc='测试编号')
    test_case_path = db.Column(db.String(256), doc='脚本路径')
    test_case_pk = db.Column(db.String(128), doc='脚本编码')
    marks = db.Column(db.String(64), doc='标记')
    has_test_case = db.Column(db.Boolean, default=False, doc='是否存在测试用例')

    # 用于序列化模型数据
    def to_dict(self):
        return {
            'id': self.id,
            'create_time': self.create_time,
            'update_time': self.update_time,
            'author': self.author,
            'comments': self.comments,
            'time': self.time,
            'epic': self.epic,
            'feature': self.feature,
            'story': self.story,
            'title': self.title,
            'source_code': self.source_code,
            'test_case_doc': self.test_case_doc,
            'test_case_full_name': self.test_case_full_name,
            'test_case_id': self.test_case_id,
            'test_case_path': self.test_case_path,
            'test_case_pk': self.test_case_pk,
            'marks': self.marks,
            'has_test_case': self.has_test_case
        }


class TestTask(BaseModel):
    """测试任务"""
    __tablename__ = 'testtask'
    task_name = db.Column(db.String(64), doc='任务名称')
    task_id = db.Column(db.String(64), unique=True, doc='任务编号')
    # build_number = db.Column(db.Integer, doc='构建编号')
    result = db.Column(db.String(32), doc='执行结果')
    duration = db.Column(db.String(32), doc='执行时长')
    timestamp = db.Column(db.String(32), doc='执行开始时间')
    full_display_name = db.Column(db.String(128), doc='jenkins构建全称')

    def to_dict(self):
        return {
            'id': self.id,
            'create_time': self.create_time,
            'update_time': self.update_time,
            'task_name': self.task_name,
            'task_id': self.task_id,
            'result': self.result,
            'duration': self.duration,
            'timestamp': self.timestamp,
            'full_display_name': self.full_display_name
        }


class TestResult(BaseModel):
    """测试结果"""
    __tablename__ = 'testresult'
    name = db.Column(db.String(64), doc='用例名称')
    status = db.Column(db.String(16), doc='执行结果')
    start = db.Column(db.String(128), doc='开始时间')
    stop = db.Column(db.String(128), doc='结束时间')
    testCaseId = db.Column(db.String(128), doc='testcasepk')
    fullName = db.Column(db.String(256), doc='测试全名')
    epic = db.Column(db.String(64), doc='项目')
    feature = db.Column(db.String(64), doc='模块')
    story = db.Column(db.String(64), doc='子模块')
    suite = db.Column(db.String(64), doc='测试编号')
    subSuite = db.Column(db.String(64), doc='测试类名')
    taskName = db.Column(db.String(128), doc='任务名称')
    taskId = db.Column(db.String(128), doc='任务Id')
    message = db.Column(db.TEXT, doc='异常信息')
    trace = db.Column(db.TEXT, doc='异常堆栈信息')
    analyze = db.Column(db.TEXT, doc='结果分析')
    analyze_status = db.Column(db.Boolean, doc='分析状态')

    def to_dict(self):
        return {
            'id': self.id,
            'create_time': self.create_time,
            'update_time': self.update_time,
            'name': self.name,
            'status':self.status,
            'start': self.start,
            'stop': self.stop,
            'testCaseId': self.testCaseId,
            'fullName': self.fullName,
            'epic': self.epic,
            'feature': self.feature,
            'story': self.story,
            'suite': self.suite,
            'subSuite': self.subSuite,
            'taskName': self.taskName,
            'taskId': self.taskId,
            'message': self.message,
            'trace': self.trace,
            'analyze': self.analyze,
            'analyze_status': self.analyze_status
        }
