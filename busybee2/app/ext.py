#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# @Time    : 2020/12/10 19:17
# @Author  : Wang Feng
# @FileName: ext.py
# @Software: PyCharm
# import redis
from flask_apscheduler import APScheduler
from flask_migrate import Migrate
# from flask_session import Session
from flask_sqlalchemy import SQLAlchemy

from app.utils.log import Logger

db = SQLAlchemy()
migrate = Migrate()
# 创建APScheduler定时任务调度器对象
scheduler = APScheduler()
# redis_store = None
logger = Logger()


def init_ext(app):
    # 使用app初始化db
    db.init_app(app)

    # 初始化migrate
    migrate.init_app(app, db)

    # 初始化APScheduler
    scheduler.init_app(app)

    # 初始化redis工具
    # global redis_store
    # redis_store = redis.StrictRedis(host=app.config.get('REDIS_HOST'), port=app.config.get('REDIS_PORT'))

    # 利用flask-session，将session数据保存到redis中
    # Session(app)

    logger.init_app(app)
