# #!/usr/bin/env python3
# # -*- coding: utf-8 -*-
# # @Time    : 2020/11/25 15:51
# # @Author  : Wang Feng
# # @FileName: verify_code.py
# # @Software: PyCharm
# import random
#
# from flask import current_app, jsonify, make_response, Blueprint
#
# # GET 127.0.0.1/api/v1.0/image_codes/<image_code_id>
# # from app.libs.ronglian_sms_sdk.SendMessage import send_message
# from app import constants
# # from app.ext import redis_store
# from app.ext import redis_store
# from app.utils.captcha.captcha import captcha
# from app.utils.response_code import RET
#
# verify_code = Blueprint('verify_code', __name__)
#
#
# @verify_code.route('/image_codes/<image_code_id>')
# def get_image_code(image_code_id):
#     """
#     获取图片验证码
#     :param image_code_id: 图片验证码编号
#     :return: 正常：验证码图片   异常：返回json
#     """
#     # 1.获取参数
#     # 2.检验参数
#     # 3.业务逻辑处理
#     # 生成验证码图片
#     name, text, image_data = captcha.generate_captcha()
#
#     # 使用哈希维护有效期的时候只能整体设置
#     # 单条维护记录，选用字符串
#     # 将验证码真实值与编号保存到redis中，设置有效期
#     # "image_code_编号1": "真实值"
#     # "image_code_编号2": "真实值"
#     # redis_store.set("image_code_%s" % image_code_id, text)
#     # redis_store.expire("image_code_%s" % image_code_id, constants.IMAGE_CODE_REDIS_EXPIRES)
#     #                   记录名字                        有效期                               值
#     try:
#         redis_store.setex("image_code_%s" % image_code_id, constants.IMAGE_CODE_REDIS_EXPIRES, text)
#         pass
#     except Exception as e:
#         # 记录日志
#         current_app.logger.error(e)
#         return jsonify(errno=RET.DBERR, errmsg="保存图片验证码信息失败")
#     # 4.返回值
#     resp = make_response(image_data)
#     resp.headers['Content-Type'] = 'image/jpg'
#     return resp
