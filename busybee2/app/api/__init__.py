#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# @Time    : 2020/12/10 19:22
# @Author  : Wang Feng
# @FileName: views.py
# @Software: PyCharm
from app.api.taskplan import taskplan
from app.api.testcase import testcase
# from app.api.passport import passport
from app.api.testscript import testscript
from app.api.testtask import testtask
from app.api.testresult import testresult
# from app.api.verify_code import verify_code


def init_view(app):
    # app.register_blueprint(verify_code, url_prefix='/verify_code')
    # app.register_blueprint(passport, url_prefix='/passport')
    app.register_blueprint(testcase, url_prefix='/testcase')
    app.register_blueprint(testscript, url_prefix='/testscript')
    app.register_blueprint(testtask, url_prefix='/testtask')
    app.register_blueprint(testresult, url_prefix='/testresult')
    app.register_blueprint(taskplan, url_prefix='/taskplan')

