#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# @Time    : 2021/1/23 11:37
# @Author  : Wang Feng
# @FileName: testresult.py
# @Software: PyCharm
from flask import Blueprint, request, jsonify

from app.models import TestResult
from app.utils.response_code import RET

testresult = Blueprint('testresult', __name__)


@testresult.route('/update', methods=['POST'])
def update():
    testresult = request.json
    name = testresult.get('name')
    status = testresult.get('status')
    start = testresult.get('start')
    stop = testresult.get('stop')
    testCaseId = testresult.get('testCaseId')
    fullName = testresult.get('fullName')
    labels = testresult.get('labels')
    taskName = testresult.get('taskName')
    taskId = testresult.get('taskId')
    statusDetails = testresult.get('statusDetails')
    epic = None
    feature = None
    story = None
    suite = None
    subSuite = None
    if labels is not None:
        for label in labels:
            if label.get('name') == 'epic':
                epic = label.get('value')
            if label.get('name') == 'feature':
                feature = label.get('value')
            if label.get('name') == 'story':
                story = label.get('value')
            if label.get('name') == 'suite':
                suite = label.get('value')
            if label.get('name') == 'subSuite':
                subSuite = label.get('value')
    # print(statusDetails)
    if statusDetails is not None:
        message = statusDetails.get('message')
        trace = statusDetails.get('trace')
    else:
        message = None
        trace = None
    # 如果测试id和任务id都相同，则是同一条数据，进行更新操作
    result = TestResult.query.filter_by(testCaseId=testCaseId, taskId=taskId).first()
    # 如果查询不存在则进行新增操作
    if result is None:
        result = TestResult()
    result.name = name
    result.status = status
    result.start = start
    result.stop = stop
    result.testCaseId = testCaseId
    result.fullName = fullName
    result.epic = epic
    result.feature = feature
    result.story = story
    result.suite = suite
    result.subSuite = subSuite
    result.taskName = taskName
    result.taskId = taskId
    result.message = message
    result.trace = trace
    if not result.save():
        return jsonify(code=RET.DBERR, msg='数据插入失败')
    return jsonify(code=RET.OK, msg='数据插入成功')


@testresult.route('/list', methods=['POST'])
def list_testresult():
    epic = request.form.get('epic', type=str)
    feature = request.form.get('feature', type=str)
    story = request.form.get('story', type=str)
    suite = request.form.get('suite', type=str)
    status = request.form.get('status', type=str)
    name = request.form.get('name')
    taskId = request.form.get('task_id', type=str)
    page_num = request.form.get('page_num', 1, type=int)
    page_size = request.form.get('page_size', 10, type=int)
    filter_list = []
    if epic:
        filter_list.append(TestResult.epic.contains(epic))
    if feature:
        filter_list.append(TestResult.feature.contains(feature))
    if story:
        filter_list.append(TestResult.story.contains(story))
    if suite:
        filter_list.append(TestResult.suite.contains(suite))
    if name:
        filter_list.append(TestResult.name.contains(name))
    if status:
        filter_list.append(TestResult.status == status)
    if taskId:
        filter_list.append(TestResult.taskId.contains(taskId))
    pagination = TestResult.query.filter(*filter_list).order_by(TestResult.id).paginate(page=page_num, per_page=page_size)
    total_page = pagination.pages
    count = pagination.total
    testresults = pagination.items
    data = list(map(lambda test_result: test_result.to_dict(), testresults))
    return jsonify(code=RET.OK, msg='查询成功', total_page=total_page, count=count, data=data)