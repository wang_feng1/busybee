#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# @Time    : 2020/11/25 17:01
# @Author  : Wang Feng
# @FileName: constants.py
# @Software: PyCharm

# 图片验证码的redis有效期
IMAGE_CODE_REDIS_EXPIRES = 180

# 短信验证码的redis有效期， 单位：秒
SMS_CODE_REDIS_EXPIRES = 300

# 发送短信验证码的间隔
SEND_SMS_CODE_INTERVAL = 60

# 登录错误常熟次数
LOGIN_ERROE_MAX_TIMES = 5

# 登录错误限制时间,单位：s
LOGIN_ERROR_FORBID_TIME = 600