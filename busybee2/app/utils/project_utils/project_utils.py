#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# @Time    : 2020/10/21 15:16
# @Author  : Wang Feng
# @FileName: project_utils.py
# @Software: PyCharm
import os

from app.settings import PROJECT_TMP_DIR
from app.utils.e2tc_utils import E2TCUtils
from app.utils.project_utils.render_utils import RenderUtils


class ProjectUtils:

    @staticmethod
    def create_project(test_cases):
        root_path_set = set()
        module_path_set = set()
        page_path_set = set()
        test_suit_conftest_path_set = set()
        data_yml_path_set = set()
        init_path_set = set()
        for test_case in test_cases:
            # 工程根目录
            root_path_set.add(os.path.join(PROJECT_TMP_DIR, test_case['epic']))
            # module目录
            module_path_set.add(os.path.join(PROJECT_TMP_DIR, test_case['epic'], 'module'))
            # page目录
            page_path_set.add(os.path.join(PROJECT_TMP_DIR, test_case['epic'], 'page', test_case['page']))
            test_case_dir = os.path.join(PROJECT_TMP_DIR, test_case['epic'], test_case['feature'],
                                         test_case['story'])
            test_case_path = os.path.join(test_case_dir, test_case['test_case_id'] + '.py')
            test_suit_conftest_path_set.add(os.path.join(test_case_dir, 'conftest.py'))
            data_yml_path_set.add(os.path.join(test_case_dir, 'data.yml'))
            init_path_set.add(os.path.join(test_case_dir, '__init__.py'))
            if not os.path.exists(test_case_dir):
                print(test_case_dir)
                os.makedirs(test_case_dir)
            ProjectUtils.create_test_case(test_case_path, test_case)
        ProjectUtils.create_module(module_path_set)
        ProjectUtils.create_page(page_path_set)
        ProjectUtils.create_root_config_file(root_path_set)
        ProjectUtils.create_test_suit_conftest(test_suit_conftest_path_set)
        ProjectUtils.create_data_yml(data_yml_path_set)
        ProjectUtils.create_init(init_path_set)

    @staticmethod
    def create_ui_project(path):
        root_path_set = set()
        module_path_set = set()
        page_path_set = set()
        test_suit_conftest_path_set =set()
        data_yml_path_set = set()
        init_path_set = set()
        test_cases = E2TCUtils.to_test_cases(path)
        for test_case in test_cases:
            test_case['test_case_id'] = test_case['testCaseID']
            test_case['is_auto'] = test_case['isAuto']
            # 工程根目录
            root_path_set.add(os.path.join(PROJECT_TMP_DIR, test_case['epic']))
            # module目录
            module_path_set.add(os.path.join(PROJECT_TMP_DIR, test_case['epic'], 'module'))
            # page目录
            page_path_set.add(os.path.join(PROJECT_TMP_DIR, test_case['epic'], 'page', test_case['page']))
            test_case_dir = os.path.join(PROJECT_TMP_DIR, test_case['epic'], test_case['feature'], test_case['story'])
            test_case_path = os.path.join(test_case_dir, test_case['testCaseID']+'.py')
            test_suit_conftest_path_set.add(os.path.join(test_case_dir, 'conftest.py'))
            data_yml_path_set.add(os.path.join(test_case_dir, 'data.yml'))
            init_path_set.add(os.path.join(test_case_dir, '__init__.py'))
            if not os.path.exists(test_case_dir):
                os.makedirs(test_case_dir)
            ProjectUtils.create_test_case(test_case_path, test_case)
        ProjectUtils.create_module(module_path_set)
        ProjectUtils.create_page(page_path_set)
        ProjectUtils.create_root_config_file(root_path_set)
        ProjectUtils.create_test_suit_conftest(test_suit_conftest_path_set)
        ProjectUtils.create_data_yml(data_yml_path_set)
        ProjectUtils.create_init(init_path_set)

    @staticmethod
    def create_test_case(test_case_path, test_case):
        print('开始生成用例文件：%s' % test_case_path)
        with open(test_case_path, 'a+', encoding='utf-8') as f:
            f.write(RenderUtils.render_test_case(test_case))

    @staticmethod
    def create_test_suit_conftest(test_suit_conftest_path_set):
        for test_suit_conftest_path in test_suit_conftest_path_set:
            with open(test_suit_conftest_path, 'a+', encoding='utf-8') as f:
                f.write(RenderUtils.render_test_suit_conftest())

    @staticmethod
    def create_root_config_file(root_path_set):
        for root_path in root_path_set:
            root_conftest_path = os.path.join(root_path, 'conftest.py')
            pytest_ini_path = os.path.join(root_path, 'pytest.ini')

            with open(root_conftest_path, 'a+', encoding='utf-8') as f:
                f.write(RenderUtils.render_root_conftest())

            with open(pytest_ini_path, 'a+', encoding='utf-8') as f:
                f.write(RenderUtils.render_pytest_ini())

    @staticmethod
    def create_data_yml(data_yml_path_set):
        for data_yml_path in data_yml_path_set:
            with open(data_yml_path, 'a+', encoding='utf-8') as f:
                f.write(RenderUtils.render_data_yml())

    @staticmethod
    def create_init(init_path_set):
        for init_path in init_path_set:
            open(init_path, 'a+', encoding='utf-8')

    @staticmethod
    def create_page(page_path_set):
        for page_path in page_path_set:
            page_dir = os.path.dirname(page_path)
            if not os.path.exists(page_dir):
                os.makedirs(page_dir)
            page_path_name = page_path + '_page.py'
            page_name = os.path.split(page_path)[-1]
            with open(page_path_name, 'a+', encoding='utf-8') as f:
                f.write(RenderUtils.render_page(page_name))

    @staticmethod
    def create_module(module_path_set):
        for module_path in module_path_set:
            if not os.path.exists(module_path):
                os.makedirs(module_path)
            globals_path = os.path.join(module_path, 'globals.py')
            open(globals_path, 'a+', encoding='utf-8')


if __name__ == '__main__':
    ProjectUtils.create_ui_project('D:\\Program Files\\PyCharm 2019.1.3\\workspace\\flask_demo\\static\\军民融合app自动化测试用例二期.xlsx')