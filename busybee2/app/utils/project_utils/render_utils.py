#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# @Time    : 2019/11/23 16:16
# @Author  : Wang Feng
# @FileName: create_project.py
# @Software: PyCharm
import time

from jinja2 import Environment, PackageLoader, FileSystemLoader

class RenderUtils:
    # TemplateLoader = FileSystemLoader(os.path.abspath('.'))
    # # env = Environment(loader=TemplateLoader)
    # env = Environment(loader=PackageLoader('E2TC', 'statics'))

    @staticmethod
    def replace_char(string, char, index):
        """
        替换字符串中指定位置字符
        """
        string = list(string)
        string[index] = char
        return ''.join(string)

    @staticmethod
    def format_class_name(class_str):
        """
        格式化类名
        :param class_str:
        :return:
        """
        class_name = ''
        for i in class_str.split('_'):
            name = RenderUtils.replace_char(i, i[0].upper(), 0)
            # print("name: %s" % name)
            class_name += name
        return class_name

    @staticmethod
    def format_steps_and_expects(str):
        """
        转化steps&expects
        :param str:
        :return:
        """
        return str.split('\n')

    @staticmethod
    def render_test_case(test_case):
        env = Environment(loader=PackageLoader('app', 'templates/testcase-templates'))
        template = env.get_template('TestCaseTemplate.j2')
        test_case['class_name'] = RenderUtils.format_class_name(test_case['test_case_id'])
        test_case['steps'] = RenderUtils.format_steps_and_expects(test_case['steps'])
        test_case['expects'] = RenderUtils.format_steps_and_expects(test_case['expects'])
        test_case['marks'] = test_case['marks'].split(',')
        test_case['time'] = time.strftime("%Y/%m/%d %H:%M")
        return template.render(test_case)

    @staticmethod
    def render_page(page_name):
        env = Environment(loader=PackageLoader('app', 'templates/page-templates'))
        template = env.get_template('PageTemplate.j2')
        return template.render(page=RenderUtils.format_class_name(page_name))

    @staticmethod
    def render_root_conftest():
        env = Environment(loader=PackageLoader('app', 'templates/root-templates'))
        template = env.get_template('ConfTestTemplate.j2')
        return template.render()

    @staticmethod
    def render_test_suit_conftest():
        env = Environment(loader=PackageLoader('app', 'templates/testsuit-templates'))
        template = env.get_template('ConfTestTemplate.j2')
        return template.render()

    @staticmethod
    def render_pytest_ini():
        env = Environment(loader=PackageLoader('app', 'templates/root-templates'))
        template = env.get_template('Pytest.ini.j2')
        return template.render()

    @staticmethod
    def render_data_yml():
        env = Environment(loader=PackageLoader('app', 'templates/testsuit-templates'))
        template = env.get_template('Data.yml.j2')
        return template.render()


if __name__ == '__main__':
    test_case = {'epic': '军民融合app', 'feature': '需求管理', 'story': '需求管理', 'page': 'demand_detail', 'title': '查看直接采购需求详情', 'test_case_id': 'test_demand_direct_detail', 'label': '', 'marks': 'test,pro', 'description': '', 'prepare': '1.打开app\n2.需求方登陆app\n3.点击需求管理，进入需求管理页面', 'steps': '1.选中并点击直接采购需求单\n2.查看需求单详情', 'expects': '进入需求单详情页\n详情正确展示', 'severity': '高', 'isAuto': 1, 'tester': '汪峰'}
    print(RenderUtils.render_test_case(test_case))
    # print(RenderUtils.render_page('list_aaa'))
    # print(RenderUtils.render_root_conftest())
    # print(RenderUtils.render_test_suit_conftest())
    # print(RenderUtils.render_pytest_ini())
    # print(RenderUtils.render_data_yml())

