#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# @Time    : 2020/11/13 23:26
# @Author  : Wang Feng
# @FileName: tasks.py
# @Software: PyCharm
import flask_sqlalchemy

from app import settings, scheduler
from app.ext import db
from app.models import TestCase
from app.models import TestScript
from app.utils.testscript_parser.testscript_parser import get_test_case_objs


@scheduler.task('interval', id='1', name='脚本扫描', minutes=300)
def testscript_scan():
    """
    脚本扫描
    :return:
    """
    print('开始执行: 脚本扫描')
    # 在flask应用上下文中执行
    with scheduler.app.app_context():
        path_list = settings.TESTSCRIPT_SCAN_PATH
        for path in path_list:
            testscript_list = get_test_case_objs(path)
            # print(testscript_list)
            test_seript_list = []
            for testscript in testscript_list:
                test_case_pk = testscript.get('test_case_pk')
                test_script_obj = TestScript.query.filter_by(test_case_pk=test_case_pk).first()
                if test_script_obj:
                    obj = test_script_obj
                else:
                    obj = TestScript()
                obj.author = testscript.get('author')
                obj.comments = testscript.get('comments')
                obj.time = testscript.get('create_time')
                obj.epic = testscript.get('epic')
                obj.feature = testscript.get('feature')
                obj.source_code = testscript.get('source_code')
                obj.story = testscript.get('story')
                obj.test_case_doc = testscript.get('test_case_doc')
                obj.test_case_full_name = testscript.get('test_case_full_name')
                obj.test_case_id = testscript.get('test_case_id')
                obj.test_case_path = testscript.get('test_case_path')
                obj.test_case_pk = testscript.get('test_case_pk')
                obj.title = testscript.get('title')
                obj.marks = testscript.get('marks')
                test_seript_list.append(obj)
            if not TestScript.save_all(test_seript_list):
                print('保存失败')


@scheduler.task('interval', id='2', name='更新用例中脚本完成状态', minutes=300)
def update_has_test_script():
    """
    更新用例中脚本完成状态
    :return:
    """
    print("开始执行: 更新用例中脚本完成状态")
    with scheduler.app.app_context():
        pk_list = TestScript.query.with_entities(TestScript.test_case_pk).all()
        TestCase.query.filter(
            TestCase.test_case_pk.in_(list(map(lambda pk: pk[0], pk_list)))
        ).update({'has_test_script': True}, synchronize_session=False)
        db.session.commit()


@scheduler.task('interval', id='3', name='更新脚本中关联用例状态', minutes=300)
def update_has_test_case():
    """
    更新脚本中关联用例状态
    :return:
    """
    print("开始执行: 更新脚本中关联用例状态")
    with scheduler.app.app_context():
        pk_list = TestCase.query.with_entities(TestCase.test_case_pk).all()
        TestScript.query.filter(
            TestScript.test_case_pk.in_(list(map(lambda pk: pk[0], pk_list)))
        ).update({'has_test_case': True}, synchronize_session=False)

        db.session.commit()