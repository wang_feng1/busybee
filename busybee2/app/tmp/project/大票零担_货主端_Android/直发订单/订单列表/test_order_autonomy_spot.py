# -*- coding: utf-8 -*-
# @Time    : 2022/06/13 08:45
# @Author  : 汪峰
import allure
import pytest
import sys
import os
sys.path.append(os.path.abspath(os.path.join(os.path.dirname(__file__), '../..')))
import module.globals as gbl


@allure.epic("大票零担_货主端_Android")
@allure.feature("直发订单")
class TestOrderAutonomySpot(object):
    """
    测试标题: 订单列表-待接单-指定下单-现付
    用例编号: test_order_autonomy_spot
    操作步骤:
        1.待下单tab选中保存的指定下单现付订单，点击指定下单
        2.搜索并选中承运商企业，输入成本运费，点击下单
        3.支付运费
    预期结果:
        预期：进入指定下单页面
        预期：跳支付页面
        预期：运费支付成功，下单成功
    """

    def setup(self):
        self.driver = gbl.driver
        self.data = gbl.data

    def teardown(self):
        pass

    @allure.story("订单列表")
    @allure.title("订单列表-待接单-指定下单-现付")
    @pytest.mark.test
    @pytest.mark.skip(reason="no way of currently testing this")
    def test_order_autonomy_spot(self):
        with allure.step("步骤：1.待下单tab选中保存的指定下单现付订单，点击指定下单"):
            pass

        with allure.step("步骤：2.搜索并选中承运商企业，输入成本运费，点击下单"):
            pass

        with allure.step("步骤：3.支付运费"):
            pass

        with allure.step("预期：预期：进入指定下单页面"):
            pass

        with allure.step("预期：预期：跳支付页面"):
            pass

        with allure.step("预期：预期：运费支付成功，下单成功"):
            pass

        