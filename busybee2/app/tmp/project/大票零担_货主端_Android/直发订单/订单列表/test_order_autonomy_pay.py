# -*- coding: utf-8 -*-
# @Time    : 2022/06/13 08:45
# @Author  : 汪峰
import allure
import pytest
import sys
import os
sys.path.append(os.path.abspath(os.path.join(os.path.dirname(__file__), '../..')))
import module.globals as gbl


@allure.epic("大票零担_货主端_Android")
@allure.feature("直发订单")
class TestOrderAutonomyPay(object):
    """
    测试标题: 订单列表-待接单-指定下单-未支付
    用例编号: test_order_autonomy_pay
    操作步骤:
        1.待下单tab选中已下单未支付的指定下单现付订单，点击支付
        2.支付运费
    预期结果:
        预期：进入到收银台页面
        预期：支付成功，订单状态变更
    """

    def setup(self):
        self.driver = gbl.driver
        self.data = gbl.data

    def teardown(self):
        pass

    @allure.story("订单列表")
    @allure.title("订单列表-待接单-指定下单-未支付")
    @pytest.mark.test
    @pytest.mark.skip(reason="no way of currently testing this")
    def test_order_autonomy_pay(self):
        with allure.step("步骤：1.待下单tab选中已下单未支付的指定下单现付订单，点击支付"):
            pass

        with allure.step("步骤：2.支付运费"):
            pass

        with allure.step("预期：预期：进入到收银台页面"):
            pass

        with allure.step("预期：预期：支付成功，订单状态变更"):
            pass

        