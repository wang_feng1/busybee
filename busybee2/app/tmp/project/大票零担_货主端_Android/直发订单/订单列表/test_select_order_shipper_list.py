# -*- coding: utf-8 -*-
# @Time    : 2022/06/13 08:45
# @Author  : 汪峰
import allure
import pytest
import sys
import os
sys.path.append(os.path.abspath(os.path.join(os.path.dirname(__file__), '../..')))
import module.globals as gbl


@allure.epic("大票零担_货主端_Android")
@allure.feature("直发订单")
class TestSelectOrderShipperList(object):
    """
    测试标题: 订单列表筛选
    用例编号: test_select_order_shipper_list
    操作步骤:
        1.根据订单类型筛选
        2.根据订单时间筛选
        3.根据订单号筛选
        4.根据客户单号筛选
        5.姓名筛选
        6.根据电话筛选
        7.根据订单状态切换tab
    预期结果:
        预期：数据展示正确
        预期：数据展示正确
        预期：数据展示正确
        预期：数据展示正确
        预期：数据展示正确
        预期：数据展示正确
        预期：数据展示正确
    """

    def setup(self):
        self.driver = gbl.driver
        self.data = gbl.data

    def teardown(self):
        pass

    @allure.story("订单列表")
    @allure.title("订单列表筛选")
    @pytest.mark.test
    @pytest.mark.skip(reason="no way of currently testing this")
    def test_select_order_shipper_list(self):
        with allure.step("步骤：1.根据订单类型筛选"):
            pass

        with allure.step("步骤：2.根据订单时间筛选"):
            pass

        with allure.step("步骤：3.根据订单号筛选"):
            pass

        with allure.step("步骤：4.根据客户单号筛选"):
            pass

        with allure.step("步骤：5.姓名筛选"):
            pass

        with allure.step("步骤：6.根据电话筛选"):
            pass

        with allure.step("步骤：7.根据订单状态切换tab"):
            pass

        with allure.step("预期：预期：数据展示正确"):
            pass

        with allure.step("预期：预期：数据展示正确"):
            pass

        with allure.step("预期：预期：数据展示正确"):
            pass

        with allure.step("预期：预期：数据展示正确"):
            pass

        with allure.step("预期：预期：数据展示正确"):
            pass

        with allure.step("预期：预期：数据展示正确"):
            pass

        with allure.step("预期：预期：数据展示正确"):
            pass

        