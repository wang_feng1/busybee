#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# @Time    : 2020/11/20 8:29
# @Author  : Wang Feng
# @FileName: settings.py.py
# @Software: PyCharm
import os

# import redis

# 项目根目录
from apscheduler.jobstores.sqlalchemy import SQLAlchemyJobStore

BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
# 文件上传目录
UPLOAD_FOLDER = os.path.join(BASE_DIR, 'app/tmp/upload')
# 工程文件生成临时目录
PROJECT_TMP_DIR = os.path.join(BASE_DIR, 'app/tmp/project')
# 压缩文件生成临时目录
ZIP_TMP_DIR = os.path.join(BASE_DIR, 'app/tmp/zip')
# 脚本扫描路径
TESTSCRIPT_SCAN_PATH = [
    # r'D:\自动化GitLab\uiautomation_test\军民融合_test',
    # r'D:\自动化GitLab\uiautomation_test\军民融合司机小程序_test',
    # r'D:\自动化GitLab\uiautomation_test\军民融合需求方app_test'
    r'D:\jenkins\UIAutomation_test_D\大票零担_货主端_Android_test',
    r'D:\jenkins\UIAutomation_test_D\传化货运网_test\HX_test'
]

# jenkins配置
JENKINS_URI = 'http://10.51.40.30:8080/jenkins'
JENKINS_USERNAME = 'tester123'
JENKINS_PASSWORD = 'tester123'
# 支持最大上传文件
MAX_CONTENT_LENGTH = 16 * 1024 * 1024


def get_db_uri(dbinfo):
    engine = dbinfo.get('ENGINE')
    driver = dbinfo.get('DRIVER')
    user = dbinfo.get('USER')
    password = dbinfo.get('PASSWORD')
    host = dbinfo.get('HOST')
    port = dbinfo.get('PORT')
    name = dbinfo.get('NAME')
    return "{}+{}://{}:{}@{}:{}/{}".format(engine, driver, user, password, host, port, name)


class Config:
    """配置信息"""
    TESTING = False

    DEBUG = False

    SQLALCHEMY_TRACK_MODIFICATIONS = False
    SECRET_KEY = 'WF'


class DevelopConfig(Config):
    """开发模式的配置信息"""
    DEBUG = True

    # 配置日志级别
    LOG_LEVEL = "DEBUG"

    dbinfo = {
        'ENGINE': 'mysql',
        'DRIVER': 'mysqlconnector',
        'USER': 'root',
        'PASSWORD': 'root',
        'HOST': '127.0.0.1',
        'PORT': 3306,
        'NAME': 'busybee',
    }

    # 数据库
    SQLALCHEMY_DATABASE_URI = get_db_uri(dbinfo)

    # 存储位置
    SCHEDULER_JOBSTORES = {
        'default': SQLAlchemyJobStore(url=SQLALCHEMY_DATABASE_URI)
    }
    # 任务调度开关
    SCHEDULER_API_ENABLED = True
    SCHEDULER_EXECUTORS = {
        'default': {'type': 'threadpool', 'max_workers': 20}
    }

    # redis
    # REDIS_HOST = '10.51.42.13'
    # REDIS_PORT = 6379

    # flask-session配置
    # SESSION_TYPE = 'redis'
    # SESSION_REDIS = redis.StrictRedis(host=REDIS_HOST, port=REDIS_PORT)
    # 对cookie中session_id进行隐藏处理
    # SESSION_USE_SIGNER = True
    # session数据的有效期，单位秒
    # PERMANENT_SESSION_LIFETIME = 86400


class TestingConfig(Config):
    """测试模式的配置信息"""
    TESTING = True
    dbinfo = {
        'ENGINE': 'mysql',
        'DRIVER': 'pymysql',
        'USER': 'root',
        'PASSWORD': 'root',
        'HOST': 'localhost',
        'PORT': 3306,
        'NAME': 'flask_demo',
    }
    SQLALCHEMY_DATABASE_URI = get_db_uri(dbinfo)


class StagingConfig(Config):
    """演示模式的配置信息"""
    dbinfo = {
        'ENGINE': 'mysql',
        'DRIVER': 'pymysql',
        'USER': 'root',
        'PASSWORD': 'root',
        'HOST': 'localhost',
        'PORT': 3306,
        'NAME': 'flask_demo',
    }
    SQLALCHEMY_DATABASE_URI = get_db_uri(dbinfo)


class ProductConfig(Config):
    """生产模式的配置信息"""
    dbinfo = {
        'ENGINE': 'mysql',
        'DRIVER': 'pymysql',
        'USER': 'root',
        'PASSWORD': 'root',
        'HOST': 'localhost',
        'PORT': 3306,
        'NAME': 'flask_demo',
    }
    SQLALCHEMY_DATABASE_URI = get_db_uri(dbinfo)


envs = {
    'develop': DevelopConfig,
    'testing': TestingConfig,
    'staging': StagingConfig,
    'product': ProductConfig,
    'default': DevelopConfig
}
