### 接口文档

------

#### 1、表结构设计

```mysql
CREATE TABLE `testcase` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '测试用例id',
  `created_at` datetime DEFAULT NULL COMMENT '创建时间',
  `updated_at` datetime DEFAULT NULL COMMENT '更新时间',
  `is_deleted` tinyint(1) DEFAULT NULL COMMENT '是否删除',
  `epic` varchar(64) DEFAULT NULL COMMENT '项目',
  `feature` varchar(64) DEFAULT NULL COMMENT '模块',
  `story` varchar(64) DEFAULT NULL COMMENT '子模块',
  `title` varchar(64) DEFAULT NULL COMMENT '用例标题',
  `page` varchar(64) DEFAULT NULL COMMENT '页面',
  `marks` varchar(64) DEFAULT NULL COMMENT '标记',
  `label` varchar(64) DEFAULT NULL COMMENT '标签',
  `is_auto` tinyint(1) DEFAULT NULL COMMENT '是否自动化',
  `has_test_script` tinyint(1) DEFAULT NULL COMMENT '是否完成脚本编写',
  `steps` text COMMENT '操作步骤',
  `expects` text COMMENT '期望结果',
  `description` text COMMENT '描述',
  `prepare` varchar(256) DEFAULT NULL COMMENT '前置条件',
  `severity` varchar(4) DEFAULT NULL COMMENT '优先级',
  `test_case_id` varchar(64) DEFAULT NULL COMMENT '测试编号',
  `tester` varchar(32) DEFAULT NULL COMMENT '测试人员',
  `test_case_full_name` varchar(512) DEFAULT NULL COMMENT '用例名称',
  `test_case_pk` varchar(128) DEFAULT NULL COMMENT '用例编码',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=92 DEFAULT CHARSET=utf8 COMMENT='测试用例表';
```

```mysql
CREATE TABLE `testscript` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '测试脚本id',
  `created_at` datetime DEFAULT NULL COMMENT '创建时间',
  `updated_at` datetime DEFAULT NULL COMMENT '更新时间',
  `is_deleted` tinyint(1) DEFAULT NULL COMMENT '是否删除',
  `author` varchar(20) DEFAULT NULL COMMENT '作者',
  `comments` varchar(256) DEFAULT NULL COMMENT '注解',
  `time` datetime DEFAULT NULL COMMENT '脚本生成时间',
  `epic` varchar(64) DEFAULT NULL COMMENT '项目',
  `feature` varchar(64) DEFAULT NULL COMMENT '模块',
  `story` varchar(64) DEFAULT NULL COMMENT '子模块',
  `title` varchar(64) DEFAULT NULL COMMENT '用例标题',
  `source_code` text COMMENT '测试源码',
  `test_case_doc` text COMMENT '测试用例文档',
  `test_case_full_name` varchar(512) DEFAULT NULL COMMENT '测试用例全称',
  `test_case_id` varchar(256) DEFAULT NULL COMMENT '测试编号',
  `test_case_path` varchar(256) DEFAULT NULL COMMENT '测试用例路径',
  `test_case_pk` varchar(128) DEFAULT NULL COMMENT '测试用例编码',
  `marks` varchar(64) DEFAULT NULL COMMENT '标记',
  `has_test_case` tinyint(1) DEFAULT NULL COMMENT '是否存在测试用例',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=692 DEFAULT CHARSET=utf8 COMMENT='测试脚本表';
```

```mysql
CREATE TABLE `testtask` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '测试任务id',
  `created_at` datetime DEFAULT NULL COMMENT '创建时间',
  `updated_at` datetime DEFAULT NULL COMMENT '更新时间',
  `is_deleted` tinyint(1) DEFAULT NULL COMMENT '是否删除',
  `task_name` varchar(64) DEFAULT NULL COMMENT '任务名称',
  `task_id` varchar(64) DEFAULT NULL COMMENT '任务id',
  `result` varchar(32) DEFAULT NULL COMMENT '执行结果',
  `duration` varchar(32) DEFAULT NULL COMMENT '执行时长',
  `timestamp` varchar(32) DEFAULT NULL COMMENT '执行开始时间',
  `full_display_name` varchar(128) DEFAULT NULL COMMENT 'jenkins构建全名',
  PRIMARY KEY (`id`),
  UNIQUE KEY `task_id` (`task_id`)
) ENGINE=InnoDB AUTO_INCREMENT=295 DEFAULT CHARSET=utf8 COMMENT='测试任务表';
```

```mysql
CREATE TABLE `testresult` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '测试结果id',
  `created_at` datetime DEFAULT NULL COMMENT '创建时间',
  `updated_at` datetime DEFAULT NULL COMMENT '更新时间',
  `is_deleted` tinyint(1) DEFAULT NULL COMMENT '是否删除',
  `name` varchar(64) DEFAULT NULL COMMENT '用例名称',
  `status` varchar(16) DEFAULT NULL COMMENT '执行结果',
  `start` varchar(128) DEFAULT NULL COMMENT '开始时间',
  `stop` varchar(128) DEFAULT NULL COMMENT '结束时间',
  `testCaseId` varchar(128) DEFAULT NULL COMMENT 'testcasepk',
  `fullName` varchar(256) DEFAULT NULL COMMENT '测试用例全名',
  `epic` varchar(64) DEFAULT NULL COMMENT '项目',
  `feature` varchar(64) DEFAULT NULL COMMENT '模块',
  `story` varchar(64) DEFAULT NULL COMMENT '子模块',
  `suite` varchar(64) DEFAULT NULL COMMENT '测试编号',
  `subSuite` varchar(64) DEFAULT NULL COMMENT '测试类名',
  `taskName` varchar(128) DEFAULT NULL COMMENT '任务名称',
  `taskId` varchar(128) DEFAULT NULL COMMENT '任务Id',
  `message` text COMMENT '异常信息',
  `trace` text COMMENT '异常堆栈信息',
  `analyze` text COMMENT '结果分析',
  `analyze_status` tinyint(1) DEFAULT NULL COMMENT '分析状态',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1825 DEFAULT CHARSET=utf8 COMMENT='测试结果表';
```

#### 2、接口说明

- 接口基准地址：`http://127.0.0.1:5000/`
- 服务端已开启 CORS 跨域支持
- 使用 HTTP Status Code 标识状态
- 数据返回格式统一使用 JSON

###### 2.1、通用返回状态说明

| 状态码 | 含义       | 说明                   |
| ------ | ---------- | ---------------------- |
| 200    | OK         | 成功                   |
| 4001   | DBERR      | 数据库查询错误         |
| 4002   | NODATA     | 无数据                 |
| 4003   | DATAEXIST  | 数据已存在             |
| 4004   | DATAERR    | 数据错误               |
| 4101   | SESSIONERR | 用户未登录             |
| 4102   | LOGINERR   | 用户登录失败           |
| 4103   | PARAMERR   | 参数错误               |
| 4104   | USERERR    | 用户不存在或未激活     |
| 4105   | ROLEERR    | 用户身份错误           |
| 4106   | PWDERR     | 密码错误               |
| 4201   | REQERR     | 非法请求或请求次数受限 |
| 4202   | IPERR      | IP受限                 |
| 4301   | THIRDERR   | 第三方系统错误         |
| 4302   | IOERR      | 文件读写错误           |
| 4500   | SERVERERR  | 内部错误               |
| 4501   | UNKOWNERR  | 未知错误               |

#### 3、接口列表

##### 3.1、测试用例

###### 3.1.1、测试用例列表

- 请求路径：testcase/list

- 请求方法：post

  Content-Type: application/x-www-form-urlencoded

- 请求参数

  | 参数            | 数据类型 | 参数说明         | 备注          |
  | --------------- | -------- | ---------------- | ------------- |
  | page_num        | int      | 分页页码         |               |
  | page_size       | int      | 分页每页条数     |               |
  | epic            | str      | 项目             |               |
  | feature         | str      | 模块             |               |
  | story           | str      | 子模块           |               |
  | title           | str      | 用例标题         |               |
  | test_case_id    | str      | 测试编号         |               |
  | has_test_script | boolean  | 是否完成脚本编写 |               |
  | marks           | str      | 标签             | test/pro/core |

###### 3.1.2、删除测试用例

- 请求路径：testcase/delete

- 请求方法：post

  Content-Type: application/x-www-form-urlencoded

- 请求参数

  | 参数 | 数据类型 | 参数说明   | 备注 |
  | ---- | -------- | ---------- | ---- |
  | id   | int      | 测试用例id |      |

- 响应

  ```json
  
  ```

###### 3.1.3、上传用例文件

- 请求路径：testcase/upload

- 请求方法：post

  Content-Type: multipart/form-data; boundary=----WebKitFormBoundary8fjKfTZ9FkAKrFy8

- 请求参数

  ```http
  ------WebKitFormBoundary8fjKfTZ9FkAKrFy8
  Content-Disposition: form-data; name="file"; filename="军民融合app自动化测试用例二期.xlsx"
  Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet
  
  
  ------WebKitFormBoundary8fjKfTZ9FkAKrFy8--
  ```

  

###### 3.1.4、导出测试工程

- 请求路径：testcase/export_project

- 请求方法：post
  Content-Type: application/json

- 请求参数

  ```json
  [
          {
              "created_at": "Thu, 03 Dec 2020 14:35:50 -0000",
              "description": "",
              "epic": "军民融合web",
              "expects": "登陆成功，可以查看到登陆用户",
              "feature": "基础服务",
              "has_test_script": false,
              "id": 1,
              "is_auto": true,
              "is_deleted": false,
              "label": "",
              "marks": "test,core,pro",
              "page": "login",
              "prepare": "",
              "severity": "高",
              "steps": "1.打开登陆页面
  2.输入需求方手机号
  3.点击获取验证码
  4.输入验证码
  5.点击登陆",
              "story": "用户登陆",
              "test_case_full_name": "基础服务.用户登陆.test_requirementLogin.TestRequirementLogin#test_requirementLogin",
              "test_case_id": "test_requirementLogin",
              "test_case_pk": "3333135b8dc531fa71f961cc48d03660",
              "tester": "",
              "title": "需求方登陆",
              "updated_at": "Thu, 03 Dec 2020 14:35:50 -0000"
          }
      ]
  ```
  
- 响应

  ```
  
  ```

  

##### 3.2、测试脚本

###### 3.2.1、测试脚本列表		

- 请求路径：testscript/list

- 请求方法：post
  Content-Type: application/x-www-form-urlencoded

- 请求参数

  | 参数          | 数据类型 | 参数说明         | 备注          |
  | ------------- | -------- | ---------------- | ------------- |
  | epic          | str      | 项目             |               |
  | feature       | str      | 模块             |               |
  | story         | str      | 子模块           |               |
  | title         | str      | 用例标题         |               |
  | test_case_id  | str      | 测试编号         |               |
  | has_test_case | boolean  | 是否存在测试用例 |               |
  | marks         | str      | 标签             | test/pro/core |
  | page_num      | int      | 分页页码         |               |
  | page_size     | int      | 分页每页条数     |               |

  

###### 3.2.2、查看测试脚本详情

- 请求路径：testscript/get_detail_by_id

- 请求方法：get

- 请求参数

  | 参数 | 数据类型 | 参数说明 | 备注 |
  | ---- | -------- | -------- | ---- |
  | id   | int      | 脚本id   |      |

  

###### 3.2.3、批量执行测试脚本

- 请求路径：testscript/execute

- 请求方法：post
  Content-Type: application/x-www-form-urlencoded

- 请求参数

  | 参数        | 数据类型 | 参数说明     | 备注 |
  | ----------- | -------- | ------------ | ---- |
  | testscripts | str      | 执行的脚本   |      |
  | task_name   | str      | 执行任务名称 |      |

  

##### 3.3、测试任务

3.3.1 

##### 3.4、测试结果

3.4.1、查询测试任务列表

- 请求路径：testtask/list

- 请求方法：post
  Content-Type: application/x-www-form-urlencoded

- 请求参数

  | 参数      | 数据类型 | 参数说明 | 备注 |
  | --------- | -------- | -------- | ---- |
  | task_name | str      | 任务名称 |      |
  | result    | str      | 测试结果 |      |

##### 3.5、定时任务

###### 3.5.1、脚本扫描

​	扫描指定工程下的脚本

- testscript_scan



###### 3.5.2、更新用例中脚本完成状态

- update_has_test_script




###### 3.5.3、更新脚本中关联用例状态

- update_has_test_case



