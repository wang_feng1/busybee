# busybee

#### 介绍
flask+vue2+elementUI自动化用例编写，脚本扫描，测试任务执行
1.用例导入
2.脚本扫描
3.支持指定脚本，创建定时任务执行
4.支持allure结果存储，查看测试结果

#### 软件架构
软件架构说明
flask+vue2+elementUI
配合allure改造，jenkins创建参数化配置任务，将测试用例结果上传到jenkins

#### 安装教程

1.  busybee2 flask项目

```
cd busybee2
# 安装
pip install -r requirements.txt
# 数据库迁移
python manage.py db init
python manage.py db migrate
python manage.py db upgrade
# flask项目启动
python manage.py runserver -h 0.0.0.0 -p 5000
```

2.  busybee2_vue vue2项目，安装node，vue2

```
cd busybee2_vue
# 安装开发依赖
npm install
# 项目启动
npm run serve
```


#### 使用说明

1. 用例管理
![输入图片说明](busybee2/doc/Snipaste_2022-07-20_09-38-09.png)
![输入图片说明](busybee2/doc/Snipaste_2022-07-20_09-39-57.png)
2. 脚本管理
![输入图片说明](busybee2/doc/Snipaste_2022-07-20_09-41-14.png)
![输入图片说明](busybee2/doc/Snipaste_2022-07-20_09-42-37.png)
3. 任务计划
![输入图片说明](busybee2/doc/Snipaste_2022-07-20_09-43-28.png)
4. 测试结果(需要jenkins配置任务，修改python allure包)
![输入图片说明](busybee2/doc/Snipaste_2022-07-20_09-44-47.png)
![输入图片说明](busybee2/doc/Snipaste_2022-07-20_09-45-55.png)

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
