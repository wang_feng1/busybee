import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '@/components/Home'
import Welcome from '@/components/Welcome'
import Testcase from '@/components/Testcase'
import Testscript from '@/components/Testscript'
import Testtask from '@/components/Testtask'
import Taskplan from '@/components/Taskplan'
import Testresult from '@/components/Testresult'


Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    redirect: '/home'
  },
  {
    path: '/home',
    component: Home,
    redirect: '/welcome',
    children: [
      {
        path: '/welcome',
        component: Welcome
      },
      {
        path: '/testcase',
        component: Testcase
      },
      {
        path: '/testscript',
        component: Testscript
      },
      {
        path: '/testtask',
        component: Testtask
      },
      {
        path: '/taskplan',
        component: Taskplan
      },
      {
        path: '/testresult',
        component: Testresult
      }
    ]
  }
]

const router = new VueRouter({
  routes
})

export default router
