import Vue from 'vue'
import App from './App.vue'
import router from './router'
import './plugins/element.js'
import axios from 'axios'
import qs from 'qs';
import moment from 'moment'
import Common from './common'
// 导入全局样式表
import './assets/css/global.css'
// 配置请求的根路径
axios.defaults.baseURL = 'http://10.51.40.30:5000'
Vue.prototype.$http = axios
Vue.prototype.$moment = moment
Vue.prototype.qs = qs
Vue.prototype.$common = Common

// Vue.filter('moment', function (value, formatString) {
//   formatString = formatString || 'YYYY-MM-DD hh:mm:ss';

//   // 普通日期(20200202)转时间
//   // return moment(value).format(formatString);

// // 这是时间戳转时间
//   return moment.unix(Number(value)).format(formatString); 
// });

Vue.config.productionTip = false

new Vue({
  router,
  render: h => h(App)
}).$mount('#app')
